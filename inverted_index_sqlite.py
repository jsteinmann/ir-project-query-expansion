import sqlite3


class InvertedIndex(object):
    def __init__(self, db_file):
        self.db = sqlite3.connect(db_file, check_same_thread=False)
        
    def close(self):
        self.db.close()
    
    def write(self):
        self.db.commit()
    
    def ensure_tables_present(self):
        try:
            self.db.execute(create_postings_table)
            self.db.execute(create_doc_lengths_table)
            self.db.commit()
            print("Tables are present")
        except Exception as e:
            print(f"The error '{e}' occurred")  

    def add_posting(self, term:str, doc_id:int, count:int):
        """Adds a posting (term and Document ID) to the index."""    
        try:
            self.db.execute(f'INSERT INTO postings (term, doc_id, count) VALUES (?, ?, ?);', (term, doc_id, count))
        except Exception as e:
            print(f"The error '{e}' occurred")
            
    def add_doc_length(self, doc_id:int, length:int):
        """Adds a document length to the index."""    
        try:
            self.db.execute(f'INSERT INTO doc_lengths (doc_id, length) VALUES (?, ?);', (doc_id, length))
        except Exception as e:
            print(f"The error '{e}' occurred")
            
    def get_term_frequency(self, term:str, doc_id:int):
        """Returns the posting list of the term from the index."""
        cursor = self.db.cursor()
        
        query = f'SELECT count FROM postings WHERE postings.term = "{term}" AND postings.doc_id = "{doc_id}";'
        
        result = None
        try:
            cursor.execute(query)
            result = cursor.fetchone()
            if result is None:
                return 0
            return result[0]
        except Exception as e:
            print(f"The error '{e}' occurred")
            
    def get_num_documents(self):
        cursor = self.db.cursor()
        
        query = f'SELECT COUNT(doc_id) FROM doc_lengths;'
        
        result = None
        try:
            cursor.execute(query)
            result = cursor.fetchone()
            return result[0]
        except Exception as e:
            print(f"The error '{e}' occurred")
            
    def get_document_frequency(self, term:str):
        cursor = self.db.cursor()
        
        query = f'SELECT COUNT(DISTINCT doc_id) FROM postings WHERE postings.term = "{term}";'
        
        result = None
        try:
            cursor.execute(query)
            result = cursor.fetchone()
            if result[0] is None or result[0] == 0:
                return 1
            return result[0]
        except Exception as e:
            print(f"The error '{e}' occurred")
        
    def get_document_length(self, doc_id:int):
        """Returns the document length from the index."""
        cursor = self.db.cursor()
        
        query = f'SELECT length FROM doc_lengths WHERE doc_id = {doc_id};'
        
        result = None
        try:
            cursor.execute(query)
            result = cursor.fetchone()
            return result[0]
        except Exception as e:
            print(f"The error '{e}' occurred")
            
    def get_average_document_length(self):
        """Returns the average document length from the index."""
        cursor = self.db.cursor()
        
        query = f'SELECT AVG(length) FROM doc_lengths;'
        
        result = None
        try:
            cursor.execute(query)
            result = cursor.fetchone()
            return result[0]
        except Exception as e:
            print(f"The error '{e}' occurred")
            
    def get_term_occurences(self, term):
        cursor = self.db.cursor()
        
        query = f'SELECT doc_id FROM postings WHERE term = "{term}";'
        
        result = None
        try:
            cursor.execute(query)
            result = cursor.fetchall()
            return [ x[0] for x in result]
        except Exception as e:
            print(f"The error '{e}' occurred")

    def is_thread_safe(self):
        cursor = self.db.cursor()
        
        query = f'SELECT * FROM pragma_compile_options WHERE compile_options LIKE "THREADSAFE=%"'
        
        result = None
        try:
            cursor.execute(query)
            result = cursor.fetchall()
            return result[0]
        except Exception as e:
            print(f"The error '{e}' occurred")

            
create_postings_table = """
CREATE TABLE IF NOT EXISTS postings (
  term TEXT NOT NULL,
  doc_id INTEGER NOT NULL,
  count INTEGER NOT NULL,
  PRIMARY KEY (term, doc_id)
);
"""

create_doc_lengths_table = """
CREATE TABLE IF NOT EXISTS doc_lengths (
  doc_id INTEGER PRIMARY KEY,
  length INTEGER NOT NULL
);
"""
