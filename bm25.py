import math

class BM25(object):
    def __init__(self, index, k, b):
        self.index = index
        self.k = k
        self.b = b
        self.N = index.get_num_documents()
        self.avg_doc_length = index.get_average_document_length()
    
    def eval_term(self, term, doc_id):
        tf = self.index.get_term_frequency(term, doc_id)
        
        a = math.log(self.N / self.index.get_document_frequency(term))
        b = (self.k + 1) * tf
        c = self.k * ((1 - self.b) + self.b * self.index.get_document_length(doc_id) / self.avg_doc_length) + tf
        
        return a * b / c
    
    
    def eval_query(self, terms, doc_id):
        result = 0
        
        for term in terms:
            result += self.eval_term(term, doc_id)
            
        return result
    
    def eval_query_weighted(self, terms, weights, doc_id):
        result = 0
        
        for (term, weight) in zip(terms, weights):
            result += weight * self.eval_term(term, doc_id)
            
        return result
    
    
    def rank_docs(self, terms, docs):
        results = []
        
        for doc_id in docs:
            results.append((doc_id, self.eval_query(terms, doc_id)))
        
        results.sort(key=lambda x: x[1],  reverse=True)
        
        return results
    
    
    def rank_docs_weighted(self, terms, weights, docs):
        results = []

        for doc_id in docs:
            results.append((doc_id, self.eval_query_weighted(terms, weights, doc_id)))

        results.sort(key=lambda x: x[1],  reverse=True)
        
        return results