import numpy as np

def DCG(query_relevancy_labels, k):
    considered_len = min(k, len(query_relevancy_labels))
    
    return np.sum( [ ql/np.log2(i + 2) for i, ql in enumerate(query_relevancy_labels[:considered_len]) ] )


# def DCG(query_relevancy_labels, k):
# # Use log with base 2
# # =======Your code=======
#     d = 0
    
#     considered_len = min(k, len(query_relevancy_labels))
#     for i in range(0, considered_len):
#         d += query_relevancy_labels[i] / math.log2(2+i)
#     return d
# # =======================

# def NDCG(query_relevancy_labels, k):
# # =======Your code=======
# # the Ideal DCG (IDCG), i.e. the maximum possible DCG value for the query
#     IDCG = DCG(np.sort(query_relevancy_labels)[::-1], k)
#     if IDCG != 0:
#         return DCG(query_relevancy_labels, k) / IDCG
#     else:
#         return DCG(query_relevancy_labels, k)
# # =======================
