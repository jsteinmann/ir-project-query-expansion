from util import pre_process
from query_expansion import pre_process_query_expand
from parse_queries import *
import metrics
from inverted_index_sqlite import *
from bm25 import BM25
from datetime import datetime



start_time = datetime.now()
print(f'start preparation at {start_time}')



index = InvertedIndex('full-index.db')
bm25 = BM25(index, 1.2, 0.75)

queries = parse_queries('data/msmarco-test2019-queries.tsv.gz')
qrels = parse_qrels('data/2019qrels-docs.txt', False)




def get_dcg(bm25, terms, docs, qrels):
    ranking = bm25.rank_docs(terms, docs)
    result = list(map(lambda x : qrels[qid].get(x[0], 0), ranking))
    return metrics.DCG(result, len(ranking))


query_to_terms_functions = [
    ('base', pre_process), # f(query) -> terms
    ('all-1', lambda q: pre_process_query_expand(q, [], 1)),
    ('all-2', lambda q: pre_process_query_expand(q, [], 2)),
    ('all-3', lambda q: pre_process_query_expand(q, [], 3)),
    ('verb-1', lambda q: pre_process_query_expand(q, ['VBP'], 1)),
    ('verb-2', lambda q: pre_process_query_expand(q, ['VBP'], 2)),
    ('verb-3', lambda q: pre_process_query_expand(q, ['VBP'], 3)),
    ('noun-1', lambda q: pre_process_query_expand(q, ['NN'], 1)),
    ('noun-2', lambda q: pre_process_query_expand(q, ['NN'], 2)),
    ('noun-3', lambda q: pre_process_query_expand(q, ['NN'], 3)),
    ('adjc-1', lambda q: pre_process_query_expand(q, ['JJ'], 1)),
    ('adjc-2', lambda q: pre_process_query_expand(q, ['JJ'], 2)),
    ('adjc-3', lambda q: pre_process_query_expand(q, ['JJ'], 3))
]


# write header
with open('experiment_result.tsv', 'a') as f:
    f.write('qid')
    
    for name, _ in query_to_terms_functions:
        f.write(f'\t{name}')
        
    f.write('\n')

print('start processing')

# compute and write values
for qid in qrels:
    qid_start = datetime.now()
    
    query = queries[qid]
    docs = list(qrels[qid].keys())
    
    optimal_order = list(qrels[qid].values())
    optimal_order.sort(reverse=True)
    dcg_optimal = metrics.DCG(optimal_order, len(optimal_order))
    
    ndcgs = []
    
    for _, f in query_to_terms_functions:
        terms = f(query)
        ndcgs.append( get_dcg(bm25, terms, docs, qrels) / dcg_optimal )
    
    with open('experiment_result.tsv', 'a') as f:
        f.write(f'{qid}')
        
        for n in ndcgs:
            f.write(f'\t{n}')
            
        f.write('\n')
        
    qid_end = datetime.now()
    
    print(f'processed query: {qid} in {qid_end - qid_start}')
        
index.close()


end_time = datetime.now()

print(f'end processing at {end_time}')
print(f'processing took {end_time - start_time}')