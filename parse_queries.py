import gzip
import csv

def parse_queries(filepath='data/msmarco-doctrain-queries.tsv.gz'):
    queries = {}
    with gzip.open(filepath, 'rt', encoding='utf8') as f:
        tsvreader = csv.reader(f, delimiter="\t")
        for [qid, querystring_of_topicid] in tsvreader:
            qid = int(qid)
            queries[qid] = querystring_of_topicid
    
    return queries


def parse_qrels(filepath="data/msmarco-doctrain-qrels.tsv.gz", zipped=True):
    def parse(f):
        qrels = {}
        
        tsvreader = csv.reader(f, delimiter=" ")
        for [qid, _, doc_id_str, rel] in tsvreader:
            doc_id = int(doc_id_str[1:])
            qid = int(qid)
            rel = int(rel)
            
            if qid in qrels:
                qrels[qid][doc_id] = rel
            else:
                qrels[qid] = {doc_id: rel}
        
        return qrels
    
    if zipped:
        with gzip.open(filepath, 'rt', encoding='utf8') as f:
            return parse(f)
    else:
        with open(filepath, 'r') as f:
            return parse(f)
    


def parse_top_100(filepath='data/msmarco-doctrain-top100.gz'):
    top_100s = {}
    
    with gzip.open(filepath, 'rt', encoding='utf8') as t:
        top100_tsvreader = csv.reader(t, delimiter=" ")
        top_100 = []
        for [qid, _, doc_id_str, rank, _, _] in top100_tsvreader:
            doc_id = int(doc_id_str[1:])
            qid = int(qid)
            
            if len(top_100) >= 100:
                query = int(qid)
                top_100s[qid] = top_100
                top_100 = []
                
            top_100.append(doc_id)
    
    return top_100s
            