from numpy.linalg import norm
import numpy as np
import sys
from nltk import pos_tag, word_tokenize
from util import pre_process

# training = 'training3'

vocab_path = 'data/metadata.tsv'
vectors_path = 'data/vectors.tsv'


def cosine_similarity(vector1, vector2):
    return np.dot(vector1, vector2)/(norm(vector1)*norm(vector2))
    
def get_top_n_words(word, n):
    with open(vocab_path, 'r') as vocab_file:
        with open(vectors_path, 'r') as vectors_file:
            vocab = vocab_file.read().split('\n')[:-1]
            if word in vocab:
                word_index = vocab.index(word)
                vectors = np.array([[float(var) for var in vector.split('\t')] for vector in vectors_file.read().split('\n')[:-1]])
                word_vector = vectors[word_index]
                cosines = [(i, cosine_similarity(word_vector, vector)) for i, vector in enumerate(vectors)]
                sorted_cosines = sorted(cosines, key=lambda x: x[1], reverse=True)
                top_n = [(vocab[i], dist) for i, dist in sorted_cosines[1:n+1]]
                return top_n
            else:
                # print('Word not in vocabulary')
                return []
                
def expand_query(query, word_class_tags, distance):
    new_query_terms = []
    tokenized_query = word_tokenize(query)
    for i, (term, tag) in enumerate(pos_tag(tokenized_query)):
        if len(word_class_tags) == 0 or tag in word_class_tags:
            new_processed_terms = pre_process(term)
            if len(new_processed_terms) > 0:
                new_terms = get_top_n_words(new_processed_terms[0], distance)
                new_query_terms += [t[0] for t in new_terms]
    return pre_process(query) + new_query_terms

def pre_process_query_expand(query, word_class_tags, distance):
    return expand_query(query, word_class_tags, distance)
    
# if len(sys.argv) > 1:
#     query_word = sys.argv[1]
#     number_top_words = 10
#     if len(sys.argv) > 2:
#         number_top_words = int(sys.argv[2])
#     with device('/device:GPU:0'):              
#         get_top_n_words(query_word, number_top_words)
# else:
#     print(f'Usage: {sys.argv[0]} query_word (number_top_words)')