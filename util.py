from nltk.stem import PorterStemmer
import re

STOPWORDS = ['a', 'an', 'and', 'are', 'as', 'at', 'be', 'but', 'by', 'for', 'if', 'in', 'into', 'is', 'it', 'no', 'not', 'of', 'on', 'or', 'such', 'that', 'the', 'their', 'then', 'there', 'these', 'they', 'this', 'to', 'was', 'will', 'with']

ps = PorterStemmer()

def pre_process(text):
    terms = []
    
    text = re.sub(r'\W+', ' ', text)
    
    # Lowercasing and stopword removal
    for term in text.split():
        term = term.lower()
        if term not in STOPWORDS:
            terms.append(ps.stem(term))
    return terms

def count_terms(terms):
    counts = {}
    
    for term in terms:
        if counts.get(term):
            counts[term] = counts[term] + 1
        else:
            counts[term] = 1
            
    return counts