from datetime import datetime
import gzip
import sys
import csv

from inverted_index_sqlite import *
from util import pre_process, count_terms

FULL_DOCS_PATH = 'data/msmarco-docs.tsv.gz'
csv.field_size_limit(sys.maxsize)

def insert_doc_fun(index):
    def task(doc):
        [doc_id_str, url, title, body] = doc
        terms = pre_process(body)
        counts = count_terms(terms)
        
        doc_id = int(doc_id_str[1:])

        for term, count in counts.items():
            index.add_posting(term, doc_id, count)
            
        index.add_doc_length(doc_id, len(terms))
    
    return task
    

            

if __name__ == "__main__":
    
    start_time = datetime.now()
    print(f'start indexing at {start_time}')
    
    index = InvertedIndex('full-index.db')
    index.ensure_tables_present()
    
    task = insert_doc_fun(index)

    with gzip.open(FULL_DOCS_PATH, 'rt', encoding='utf8') as f:
        tsvreader = csv.reader(f, delimiter="\t")
        counter = 0
        
        for doc in tsvreader:
            task(doc)
            
            counter += 1

            if counter % 10000 == 0:
                print(f'processed {counter} documents in {datetime.now() - start_time}')
                index.write()
    
    index.write()
    index.close()
    
    end_time = datetime.now()
    print(f'finish indexing at {end_time}')
    print(f'indexed {counter} documents')
    print(f'it took {end_time-start_time}')